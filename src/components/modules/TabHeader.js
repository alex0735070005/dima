import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ReportBox from './ReportBox';
import Divider from '@material-ui/core/Divider';

class TabHeader extends React.Component {
    state = {
        showReport: true,
        tab: 'Behavior',
    };

    onClick() {
        this.setState(prevState => ({ showReport: !prevState.showReport }));
    }


    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Card className={classes.card}>
                    <h2 className={classes.title}>Description {this.state.tab} Auth</h2>
                    <h4 className={classes.titleDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </h4>
                </Card>

                <Card className={classes.card}>
                    <h2 className={classes.title}>{this.state.tab} Auth Reports </h2>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 1
                            </Button>
                            <Divider className={classes.divider} />
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 2
                            </Button>
                            <Divider className={classes.divider} />
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 3
                            </Button>
                            <Divider className={classes.divider}/>
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 4
                            </Button>
                            <Divider className={classes.divider}/>
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 5
                            </Button>
                            <Divider className={classes.divider}/>
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 6
                            </Button>
                            <Divider className={classes.divider}/>
                            <Button onClick={() => this.onClick()} variant="contained" size="large" color="primary" className={classes.button}>
                                Get Auth {this.state.tab} report 7
                            </Button>
                            <Divider className={classes.divider}/>
                        </Grid>
                        <Grid item xs={6}>
                            {
                                this.state.showReport
                                    ? <ReportBox />
                                    : null
                            }
                        </Grid>
                    </Grid>
                </Card>
            </div>


        );
    }
}
TabContent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabContent);