import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import AssignmentTurnedIn from '@material-ui/icons/AssignmentTurnedIn';



const styles = theme => ({
    card: {
        minWidth: 275,
        margin: 20,
        padding: 10,
        height: '85%',
    },
    buttonView: {
        marginLeft: 'auto',
    },
    paragraph: {
        height:'80%',
        background: '#eee',
        borderRadius: '5px',
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

class ReportBox extends React.Component {

    render() {
        
        const { classes } = this.props;
        return (
            <Card className={classes.card}>
                <CardActionArea  className={classes.paragraph}>
                    <CardContent>
                        <Typography gutterBottom variant="headline" component="h2">
                            Report
                        </Typography>
                        <Typography gutterBottom variant="headline" component="h4">
                            Description Business Report
                        </Typography>
                        <Typography component="p">
                            Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                            across all continents except Antarctica
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button variant="contained" size="medium" color="primary" className={classes.buttonView}>
                        View Report
                        <AssignmentTurnedIn className={classes.rightIcon} />
                    </Button>
                </CardActions>
            </Card>
        )
    }
}

ReportBox.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ReportBox);