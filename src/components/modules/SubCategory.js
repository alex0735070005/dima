import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ReportBox from './ReportBox';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        textAlign: 'center',
    },
    card: {
        minWidth: 275,
        margin: 20,
        padding: 10,
    },
    button: {
        margin: theme.spacing.unit,
        width: "100%"
    },
    buttonView: {
        marginLeft: 'auto',
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    divider: {
        marginLeft: '16px',
    },
    titleDescription: {
        paddingLeft: '20px',
        paddingRight: '20px'
    },
});


const ShowReports = (props) => {
    if(props.items){
        return props.items.keys.map((id)=>{
            return(
                <div key={id}>
                    <Button onClick={props.onClickReport} variant="contained" size="large" color="primary" >
                        Get Auth {props.items.byId[id]['name']}
                    </Button>
                    <Divider />
                </div>)
            });
    }
    return false;
}



class SubCategory extends React.Component {
    state = {
        showReport: true,
        tab: 'Behavior',
    };

    onClick(el) {
        console.log('hi')
        //this.setState(prevState => ({ showReport: !prevState.showReport }));
    }


    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <Card className={classes.card}>
                    <h2 className={classes.title}>Description {this.state.tab} Auth</h2>
                    <h4 className={classes.titleDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </h4>
                </Card>

                <Card className={classes.card}>
                    <h2 className={classes.title}>{this.state.tab} Auth Reports </h2>
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <ShowReports items = {this.props.data.reports} onClickReport = {this.onClick}/>
                        </Grid>
                        <Grid item xs={6}>
                            {
                                this.state.showReport
                                    ? <ReportBox />
                                    : null
                            }
                        </Grid>
                    </Grid>
                </Card>
            </div>


        );
    }
}
SubCategory.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default connect(
    (state)=>({
        data:state.subCategory
    }),
    (dispach)=>({})
)(withStyles(styles)(SubCategory))