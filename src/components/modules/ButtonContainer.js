import React from "react";
import { Grid, Button, Typography } from '@material-ui/core'
import { Link } from 'react-router-dom';

const ButtonContainer = (props) => {

    return (
        <Grid item 
            xs={6} 
            container
            className="button__container">
            <Button
                className="button button__home">
                    <Link
                        className="button__home_link"
                        to={props.data.link}>{props.data.name}
                    </Link>
            </Button>
        </Grid>
    )
}

export default ButtonContainer;