import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import TabBusiness from '../modules/TabBusiness';
import TabTechnical from '../modules/TabTechnical'
import TabBehavior from '../modules/TabBehavior'


function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        
    },
    title: {
        textAlign: 'center',
    },
    card: {
        minWidth: 275,
        margin: 20,
        padding: 10,
    },
    button: {
        margin: theme.spacing.unit,
    },
    buttonView: {
        marginLeft: 'auto',
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});





class SimpleTabs extends React.Component {
    state = {
        value: 0,
        showReport: false,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Tabs value={value} onChange={this.handleChange}>
                        <Tab label="Business" />
                        <Tab label="Technical" />
                        <Tab label="Behavior" href="#basic-tabs" />
                    </Tabs>
                </AppBar>
                {value === 0 && <TabContainer> <TabBusiness /> </TabContainer>}
                {value === 1 && <TabContainer> <TabTechnical /> </TabContainer>}
                {value === 2 && <TabContainer> <TabBehavior /> </TabContainer>}
            </div>
        );
    }
}

SimpleTabs.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTabs);