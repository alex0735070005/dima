import React, {Component} from "react";
import { Grid } from '@material-ui/core';
import {connect} from 'react-redux';
import ButtonContainer from "../modules/ButtonContainer";

class Home extends Component {

    showButtons = () => {
        return this.props.categories.keys.map((id)=> <ButtonContainer key={id} data={this.props.categories.byId[id]} />)
    }

    render(){
        return( 
            <div className="section__home">
                <h1 className="section__home_title">Report Aggregator</h1>
                <Grid 
                    container spacing={24}
                    className="section__home_content">
                    {this.showButtons()}
                </Grid>
            </div>
        )
    }
}

export default connect(
    (state)=>({
        categories:state.categories
    }),
    (dispach)=>({})
)(Home)