import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {connect} from 'react-redux';
import SubCategory from '../modules/SubCategory';


const ShowNavTab = (items) => {
    if(items){
        return items.keys.map((id)=><Tab key={id} value={id} label={items.byId[id]['name']} />);
    }
    return false;
}

class Category extends React.Component {

  
    componentDidMount(){
        let slug = this.props.match.params.slug;
        let Category = this.props.categories.byId[slug];
        let children = Category.children.byId;
        this.props.updateCategory(Category);
        this.props.updateSubCategory(children[Object.keys(children)[0]]);
    }

    changeSubCategory = (e, v) => {
        console.log(55555);
        this.props.updateSubCategory(this.props.category.children.byId[v]);
    }

    render() {
      
        return (
            <div >
                <AppBar position="static">
                    <Tabs value={this.props.subcategory ? this.props.subcategory.id : 1 } onChange={this.changeSubCategory}>
                       {ShowNavTab(this.props.category.children)}
                    </Tabs>
                </AppBar>

               <SubCategory />
                 
            </div>
        );
    }
}

export default connect(
  (state)=>({
      categories:state.categories,
      category:state.category,
      subcategory:state.subCategory
  }),
  (dispatch)=>({
    updateCategory: (d) =>{
      dispatch({type: 'UPDATE_CATEGORY', data: d}) 
    },
    updateSubCategory: (d) =>{
      dispatch({type: 'UPDATE_SUBCATEGORY', data: d}) 
    }
  })
)(Category)