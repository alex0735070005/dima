const database = {
    project: {
        LandViewer: {
            id: 'Land Viewer',
            link: 'land-viewer',
        },
        Storage: {
            id: 'Storage',
            link: 'storage',
        },
        Auth: {
            id: 'Auth',
            link: 'auth',
        },
        Processing: {
            id: 'Processing',
            link: 'processing',
        },
        Vision: {
            id: 'Vision',
            link: 'vision',
        },
        Watcher: {
            id: 'Watcher',
            link: 'watcher',
        },
        Antarctica: {
            id: 'Antarctica',
            link: 'antarctica',
        },
        order: ['Land Viewer', 'Storage', 'Auth', 'Processing', 'Vision', 'Watcher', 'Antarctica']

    },
    businessTab: {
        tab1: {
            title: 'business',
            tabDescription: 'This is a business Tab of Land Viewer',
            id: 'Land Viewer',
        },
        tab2: {
            title: 'business',
            tabDescription: 'This is a business Tab of Storage',
            id: 'Storage',
        },
        tab3: {
            title: 'business',
            tabDescription: 'This is a business Tab of Auth',
            id: 'Auth',
        },
        tab4: {
            title: 'business',
            tabDescription: 'This is a business Tab of Processing',
            id: 'Processing',
        },
        tab5: {
            title: 'business',
            tabDescription: 'This is a business Tab of Vision',
            id: 'Vision',
        },
        tab6: {
            title: 'business',
            tabDescription: 'This is a business Tab of Watcher',
            id: 'Watcher',
        },
        tab7: {
            title: 'business',
            tabDescription: 'This is a business Tab of Antarctica',
            id: 'Antarctica',
        },
        order: ['Land Viewer', 'Storage', 'Auth', 'Processing', 'Vision', 'Sendy', 'Antarctica'],
    },
    technicalTab: {
        tab1: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Land Viewer',
            id: 'Land Viewer',
        },
        tab2: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Storage',
            id: 'Storage',
        },
        tab3: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Auth',
            id: 'Auth',
        },
        tab4: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Processing',
            id: 'Processing',
        },
        tab5: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Vision',
            id: 'Vision',
        },
        tab6: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Watcher',
            id: 'Watcher',
        },
        tab7: {
            title: 'technical',
            tabDescription: 'This is a technical Tab of Antarctica',
            id: 'Antarctica',
        },
        order: ['Land Viewer', 'Storage', 'Auth', 'Processing', 'Vision', 'Sendy', 'Antarctica'],
    },
    report:{
        report1:{
            title: 'Land Viewer Business rep1',
            description: 'Land Viewer Business rep1 description',
            link: '#',
        },
        report2:{
            title: 'Land Viewer Business rep1',
            description: 'Land Viewer Business rep1 description',
            link: '#',
        }
    }

}
 export default database;


        
   



