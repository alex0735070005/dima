export const mock = {
    landViewer: {
        id: 'Land Viewer',
        link: 'land-viewer',
        page: {
            tab1: {
                title: 'business',
                tabDescription: 'This is a business Tab of Land Viewer',
                id: 'business',
            },
            tab2: {
                title: 'technical',
                tabDescription: 'This is a technical Tab of Land Viewer',
                id: 'technical',
            },
            tab3: {
                title: 'behavior',
                tabDescription: 'This is a behavior Tab of Land Viewer',
                id: 'behavior',
            },
            orderTab: [tab1, tab2, tab3],
        },
        Tab_1_Reports: [
            {
                reportTitle: 'business report Land Viewer',
                reportLink: '/business-report-1',
                reportDescription: 'description  business report Land Viewer',
                reportButtonTitle: 'click to see business report-1 Land Viewer',
                subReportTitle: 'Land Viewer business Report-1 Description',
                subReportDescription: 'This is  business Report-1 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Land Viewer GO',
                subReportButtonLink: '/business-report-1/1'
            },
            {
                reportTitle: 'business report Land Viewer',
                reportLink: '/business-report-2',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-2 Land Viewer',
                subReportTitle: 'Land Viewer Report-2 Description',
                subReportDescription: 'This is Report-2 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Land Viewer GO',
                subReportButtonLink: '/business-report-2/1'
            },
            {
                reportTitle: 'business report Land Viewer',
                reportLink: '/business-report-3',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-3 Land Viewer',
                subReportTitle: 'Land Viewer Report-3 Description',
                subReportDescription: 'This is Report-3 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Land Viewer GO',
                subReportButtonLink: '/business-report-3/1'
            },
            {
                reportTitle: 'business report Land Viewer',
                reportLink: '/business-report-4',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-4 Land Viewer',
                subReportTitle: 'Land Viewer Report-4 Description',
                subReportDescription: 'This is Report-4 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Land Viewer GO',
                subReportButtonLink: '/report-4/1'
            },
            {
                reportTitle: 'business report Land Viewer',
                reportLink: '/business-report-5',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-5 Land Viewer',
                subReportTitle: 'Land Viewer Report-5 Description',
                subReportDescription: 'This is Report-5 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Land Viewer GO',
                subReportButtonLink: '/report-5/1'
            },
            {
                reportTitle: 'business report Land Viewer',
                reportLink: '/business-report-6',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-6 Land Viewer',
                subReportTitle: 'Land Viewer Report-6 Description',
                subReportDescription: 'This is Report-6 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Land Viewer GO',
                subReportButtonLink: '/report-6/1'
            },
        ],
        Tab_2_Reports: [
            {
                reportTitle: 'technical report Land Viewer',
                reportLink: '/technical-report-1',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-1 Land Viewer',
                subReportTitle: 'Land Viewer Report-1 Description',
                subReportDescription: 'This is Report-1 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Land Viewer GO',
                subReportButtonLink: '/technical-report-1/1'
            },
            {
                reportTitle: 'technical report Land Viewer',
                reportLink: '/technical-report-2',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-2 Land Viewer',
                subReportTitle: 'Land Viewer Report-2 Description',
                subReportDescription: 'This is Report-2 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Land Viewer GO',
                subReportButtonLink: '/technical-report-2/1'
            },
            {
                reportTitle: 'technical report Land Viewer',
                reportLink: '/technical-report-3',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-3 Land Viewer',
                subReportTitle: 'Land Viewer Report-3 Description',
                subReportDescription: 'This is Report-3 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Land Viewer GO',
                subReportButtonLink: '/technical-report-3/1'
            },
            {
                reportTitle: 'technical report Land Viewer',
                reportLink: '/technical-report-4',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-4 Land Viewer',
                subReportTitle: 'Land Viewer Report-4 Description',
                subReportDescription: 'This is Report-4 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Land Viewer GO',
                subReportButtonLink: '/technical-report-4/1'
            },
            {
                reportTitle: 'technical report Land Viewer',
                reportLink: '/technical-report-5',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-5 Land Viewer',
                subReportTitle: 'Land Viewer Report-5 Description',
                subReportDescription: 'This is Report-5 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Land Viewer GO',
                subReportButtonLink: '/technical-report-5/1'
            },
            {
                reportTitle: 'technical report Land Viewer',
                reportLink: '/technical-report-6',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-6 Land Viewer',
                subReportTitle: 'Land Viewer Report-6 Description',
                subReportDescription: 'This is Report-6 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Land Viewer GO',
                subReportButtonLink: '/technical-report-6/1'
            },
        ],
        Tab_3_Reports: [
            {
                reportTitle: 'behavior report Land Viewer',
                reportLink: '/behavior-report-1',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-1 Land Viewer',
                subReportTitle: 'Land Viewer Report-1 Description',
                subReportDescription: 'This is Report-1 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Land Viewer GO',
                subReportButtonLink: '/behavior-report-1/1'
            },
            {
                reportTitle: 'behavior report Land Viewer',
                reportLink: '/behavior-report-2',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-2 Land Viewer',
                subReportTitle: 'Land Viewer Report-2 Description',
                subReportDescription: 'This is Report-2 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Land Viewer GO',
                subReportButtonLink: '/behavior-report-2/1'
            },
            {
                reportTitle: 'behavior report Land Viewer',
                reportLink: '/behavior-report-3',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-3 Land Viewer',
                subReportTitle: 'Land Viewer Report-3 Description',
                subReportDescription: 'This is Report-3 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Land Viewer GO',
                subReportButtonLink: '/behavior-report-3/1'
            },
            {
                reportTitle: 'behavior report Land Viewer',
                reportLink: '/behavior-report-4',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-4 Land Viewer',
                subReportTitle: 'Land Viewer Report-4 Description',
                subReportDescription: 'This is Report-4 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Land Viewer GO',
                subReportButtonLink: '/behavior-report-4/1'
            },
            {
                reportTitle: 'behavior report Land Viewer',
                reportLink: '/behavior-report-5',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-5 Land Viewer',
                subReportTitle: 'Land Viewer Report-5 Description',
                subReportDescription: 'This is Report-5 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Land Viewer GO',
                subReportButtonLink: '/behavior-report-5/1'
            },
            {
                reportTitle: 'behavior report Land Viewer',
                reportLink: '/behavior-report-6',
                reportDescription: 'description report Land Viewer',
                reportButtonTitle: 'click to see report-6 Land Viewer',
                subReportTitle: 'Land Viewer Report-6 Description',
                subReportDescription: 'This is Report-6 from Land Viewer, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Land Viewer GO',
                subReportButtonLink: '/behavior-report-6/1'
            },
        ],
    },
    storage: {
        id: 'Storage',
        link: 'storage',
        page: {
            tab1: {
                title: 'business',
                tabDescription: 'This is a Business Tab of Storage',
                id: 'business',
            },
            tab2: {
                title: 'technical',
                tabDescription: 'This is a Technical Tab of Storage',
                id: 'technical',
            },
            tab3: {
                title: 'behavior',
                tabDescription: 'This is a Behavior Tab of Storage',
                id: 'behavior',
            },
            orderTab: [tab1, tab2, tab3],
        },
        Tab_1_Reports: [
            {
                reportTitle: 'business report Storage',
                reportLink: '/business-report-1',
                reportDescription: 'description  business report Storage',
                reportButtonTitle: 'click to see business report-1 Storage',
                subReportTitle: 'Storage business Report-1 Description',
                subReportDescription: 'This is  business Report-1 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Storage GO',
                subReportButtonLink: '/business-report-1/1'
            },
            {
                reportTitle: 'business report Storage',
                reportLink: '/business-report-2',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-2 Storage',
                subReportTitle: 'Storage Report-2 Description',
                subReportDescription: 'This is Report-2 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Storage GO',
                subReportButtonLink: '/business-report-2/1'
            },
            {
                reportTitle: 'business report Storage',
                reportLink: '/business-report-3',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-3 Storage',
                subReportTitle: 'Storage Report-3 Description',
                subReportDescription: 'This is Report-3 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Storage GO',
                subReportButtonLink: '/business-report-3/1'
            },
            {
                reportTitle: 'business report Storage',
                reportLink: '/business-report-4',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-4 Storage',
                subReportTitle: 'Storage Report-4 Description',
                subReportDescription: 'This is Report-4 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Storage GO',
                subReportButtonLink: '/report-4/1'
            },
            {
                reportTitle: 'business report Storage',
                reportLink: '/business-report-5',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-5 Storage',
                subReportTitle: 'Storage Report-5 Description',
                subReportDescription: 'This is Report-5 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Storage GO',
                subReportButtonLink: '/report-5/1'
            },
            {
                reportTitle: 'business report Storage',
                reportLink: '/business-report-6',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-6 Storage',
                subReportTitle: 'Storage Report-6 Description',
                subReportDescription: 'This is Report-6 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Storage GO',
                subReportButtonLink: '/report-6/1'
            },
        ],
        Tab_2_Reports: [
            {
                reportTitle: 'technical report Storage',
                reportLink: '/technical-report-1',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-1 Storage',
                subReportTitle: 'Storage Report-1 Description',
                subReportDescription: 'This is Report-1 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Storage GO',
                subReportButtonLink: '/technical-report-1/1'
            },
            {
                reportTitle: 'technical report Storage',
                reportLink: '/technical-report-2',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-2 Storage',
                subReportTitle: 'Storage Report-2 Description',
                subReportDescription: 'This is Report-2 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Storage GO',
                subReportButtonLink: '/technical-report-2/1'
            },
            {
                reportTitle: 'technical report Storage',
                reportLink: '/technical-report-3',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-3 Storage',
                subReportTitle: 'Storage Report-3 Description',
                subReportDescription: 'This is Report-3 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Storage GO',
                subReportButtonLink: '/technical-report-3/1'
            },
            {
                reportTitle: 'technical report Storage',
                reportLink: '/technical-report-4',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-4 Storage',
                subReportTitle: 'Storage Report-4 Description',
                subReportDescription: 'This is Report-4 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Storage GO',
                subReportButtonLink: '/technical-report-4/1'
            },
            {
                reportTitle: 'technical report Storage',
                reportLink: '/technical-report-5',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-5 Storage',
                subReportTitle: 'Storage Report-5 Description',
                subReportDescription: 'This is Report-5 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Storage GO',
                subReportButtonLink: '/technical-report-5/1'
            },
            {
                reportTitle: 'technical report Storage',
                reportLink: '/technical-report-6',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-6 Storage',
                subReportTitle: 'Storage Report-6 Description',
                subReportDescription: 'This is Report-6 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Storage GO',
                subReportButtonLink: '/technical-report-6/1'
            },
        ],
        Tab_3_Reports: [
            {
                reportTitle: 'behavior report Storage',
                reportLink: '/behavior-report-1',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-1 Storage',
                subReportTitle: 'Storage Report-1 Description',
                subReportDescription: 'This is Report-1 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Storage GO',
                subReportButtonLink: '/behavior-report-1/1'
            },
            {
                reportTitle: 'behavior report Storage',
                reportLink: '/behavior-report-2',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-2 Storage',
                subReportTitle: 'Storage Report-2 Description',
                subReportDescription: 'This is Report-2 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Storage GO',
                subReportButtonLink: '/behavior-report-2/1'
            },
            {
                reportTitle: 'behavior report Storage',
                reportLink: '/behavior-report-3',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-3 Storage',
                subReportTitle: 'Storage Report-3 Description',
                subReportDescription: 'This is Report-3 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Storage GO',
                subReportButtonLink: '/behavior-report-3/1'
            },
            {
                reportTitle: 'behavior report Storage',
                reportLink: '/behavior-report-4',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-4 Storage',
                subReportTitle: 'Storage Report-4 Description',
                subReportDescription: 'This is Report-4 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Storage GO',
                subReportButtonLink: '/behavior-report-4/1'
            },
            {
                reportTitle: 'behavior report Storage',
                reportLink: '/behavior-report-5',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-5 Storage',
                subReportTitle: 'Storage Report-5 Description',
                subReportDescription: 'This is Report-5 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Storage GO',
                subReportButtonLink: '/behavior-report-5/1'
            },
            {
                reportTitle: 'behavior report Storage',
                reportLink: '/behavior-report-6',
                reportDescription: 'description report Storage',
                reportButtonTitle: 'click to see report-6 Storage',
                subReportTitle: 'Storage Report-6 Description',
                subReportDescription: 'This is Report-6 from Storage, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Storage GO',
                subReportButtonLink: '/behavior-report-6/1'
            },
        ],
    },
    auth: {
        id: 'Auth',
        link: 'auth',
        page: {
            tab1: {
                title: 'business',
                tabDescription: 'This is a Business Tab of Auth',
                id: 'business',
            },
            tab2: {
                title: 'technical',
                tabDescription: 'This is a Technical Tab of Auth',
                id: 'technical',
            },
            tab3: {
                title: 'behavior',
                tabDescription: 'This is a Behavior Tab of Auth',
                id: 'behavior',
            },
            orderTab: [tab1, tab2, tab3],
        },
        Tab_1_Reports: [
            {
                reportTitle: 'business report Auth',
                reportLink: '/business-report-1',
                reportDescription: 'description  business report Auth',
                reportButtonTitle: 'click to see business report-1 Auth',
                subReportTitle: 'Auth business Report-1 Description',
                subReportDescription: 'This is  business Report-1 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Auth GO',
                subReportButtonLink: '/business-report-1/1'
            },
            {
                reportTitle: 'business report Auth',
                reportLink: '/business-report-2',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-2 Auth',
                subReportTitle: 'Auth Report-2 Description',
                subReportDescription: 'This is Report-2 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Auth GO',
                subReportButtonLink: '/business-report-2/1'
            },
            {
                reportTitle: 'business report Auth',
                reportLink: '/business-report-3',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-3 Auth',
                subReportTitle: 'Auth Report-3 Description',
                subReportDescription: 'This is Report-3 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Auth GO',
                subReportButtonLink: '/business-report-3/1'
            },
            {
                reportTitle: 'business report Auth',
                reportLink: '/business-report-4',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-4 Auth',
                subReportTitle: 'Auth Report-4 Description',
                subReportDescription: 'This is Report-4 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Auth GO',
                subReportButtonLink: '/report-4/1'
            },
            {
                reportTitle: 'business report Auth',
                reportLink: '/business-report-5',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-5 Auth',
                subReportTitle: 'Auth Report-5 Description',
                subReportDescription: 'This is Report-5 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Auth GO',
                subReportButtonLink: '/report-5/1'
            },
            {
                reportTitle: 'business report Auth',
                reportLink: '/business-report-6',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-6 Auth',
                subReportTitle: 'Auth Report-6 Description',
                subReportDescription: 'This is Report-6 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Auth GO',
                subReportButtonLink: '/report-6/1'
            },
        ],
        Tab_2_Reports: [
            {
                reportTitle: 'technical report Auth',
                reportLink: '/technical-report-1',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-1 Auth',
                subReportTitle: 'Auth Report-1 Description',
                subReportDescription: 'This is Report-1 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Auth GO',
                subReportButtonLink: '/technical-report-1/1'
            },
            {
                reportTitle: 'technical report Auth',
                reportLink: '/technical-report-2',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-2 Auth',
                subReportTitle: 'Auth Report-2 Description',
                subReportDescription: 'This is Report-2 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Auth GO',
                subReportButtonLink: '/technical-report-2/1'
            },
            {
                reportTitle: 'technical report Auth',
                reportLink: '/technical-report-3',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-3 Auth',
                subReportTitle: 'Auth Report-3 Description',
                subReportDescription: 'This is Report-3 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Auth GO',
                subReportButtonLink: '/technical-report-3/1'
            },
            {
                reportTitle: 'technical report Auth',
                reportLink: '/technical-report-4',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-4 Auth',
                subReportTitle: 'Auth Report-4 Description',
                subReportDescription: 'This is Report-4 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Auth GO',
                subReportButtonLink: '/technical-report-4/1'
            },
            {
                reportTitle: 'technical report Auth',
                reportLink: '/technical-report-5',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-5 Auth',
                subReportTitle: 'Auth Report-5 Description',
                subReportDescription: 'This is Report-5 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Auth GO',
                subReportButtonLink: '/technical-report-5/1'
            },
            {
                reportTitle: 'technical report Auth',
                reportLink: '/technical-report-6',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-6 Auth',
                subReportTitle: 'Auth Report-6 Description',
                subReportDescription: 'This is Report-6 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Auth GO',
                subReportButtonLink: '/technical-report-6/1'
            },
        ],
        Tab_3_Reports: [
            {
                reportTitle: 'behavior report Auth',
                reportLink: '/behavior-report-1',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-1 Auth',
                subReportTitle: 'Auth Report-1 Description',
                subReportDescription: 'This is Report-1 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Auth GO',
                subReportButtonLink: '/behavior-report-1/1'
            },
            {
                reportTitle: 'behavior report Auth',
                reportLink: '/behavior-report-2',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-2 Auth',
                subReportTitle: 'Auth Report-2 Description',
                subReportDescription: 'This is Report-2 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Auth GO',
                subReportButtonLink: '/behavior-report-2/1'
            },
            {
                reportTitle: 'behavior report Auth',
                reportLink: '/behavior-report-3',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-3 Auth',
                subReportTitle: 'Auth Report-3 Description',
                subReportDescription: 'This is Report-3 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Auth GO',
                subReportButtonLink: '/behavior-report-3/1'
            },
            {
                reportTitle: 'behavior report Auth',
                reportLink: '/behavior-report-4',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-4 Auth',
                subReportTitle: 'Auth Report-4 Description',
                subReportDescription: 'This is Report-4 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Auth GO',
                subReportButtonLink: '/behavior-report-4/1'
            },
            {
                reportTitle: 'behavior report Auth',
                reportLink: '/behavior-report-5',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-5 Auth',
                subReportTitle: 'Auth Report-5 Description',
                subReportDescription: 'This is Report-5 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Auth GO',
                subReportButtonLink: '/behavior-report-5/1'
            },
            {
                reportTitle: 'behavior report Auth',
                reportLink: '/behavior-report-6',
                reportDescription: 'description report Auth',
                reportButtonTitle: 'click to see report-6 Auth',
                subReportTitle: 'Auth Report-6 Description',
                subReportDescription: 'This is Report-6 from Auth, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Auth GO',
                subReportButtonLink: '/behavior-report-6/1'
            },
        ],
    },

    processing: {
        id: 'Processing',
        link: 'processing',
        page: {
            tab1: {
                title: 'business',
                tabDescription: 'This is a Business Tab of Processing',
                id: 'business',
            },
            tab2: {
                title: 'technical',
                tabDescription: 'This is a Technical Tab of Processing',
                id: 'technical',
            },
            tab3: {
                title: 'behavior',
                tabDescription: 'This is a Behavior Tab of Processing',
                id: 'behavior',
            },
            orderTab: [tab1, tab2, tab3],
        },
        Tab_1_Reports: [
            {
                reportTitle: 'business report Processing',
                reportLink: '/business-report-1',
                reportDescription: 'description  business report Processing',
                reportButtonTitle: 'click to see business report-1 Processing',
                subReportTitle: 'Processing business Report-1 Description',
                subReportDescription: 'This is  business Report-1 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Processing GO',
                subReportButtonLink: '/business-report-1/1'
            },
            {
                reportTitle: 'business report Processing',
                reportLink: '/business-report-2',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-2 Processing',
                subReportTitle: 'Processing Report-2 Description',
                subReportDescription: 'This is Report-2 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Processing GO',
                subReportButtonLink: '/business-report-2/1'
            },
            {
                reportTitle: 'business report Processing',
                reportLink: '/business-report-3',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-3 Processing',
                subReportTitle: 'Processing Report-3 Description',
                subReportDescription: 'This is Report-3 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Processing GO',
                subReportButtonLink: '/business-report-3/1'
            },
            {
                reportTitle: 'business report Processing',
                reportLink: '/business-report-4',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-4 Processing',
                subReportTitle: 'Processing Report-4 Description',
                subReportDescription: 'This is Report-4 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Processing GO',
                subReportButtonLink: '/report-4/1'
            },
            {
                reportTitle: 'business report Processing',
                reportLink: '/business-report-5',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-5 Processing',
                subReportTitle: 'Processing Report-5 Description',
                subReportDescription: 'This is Report-5 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Processing GO',
                subReportButtonLink: '/report-5/1'
            },
            {
                reportTitle: 'business report Processing',
                reportLink: '/business-report-6',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-6 Processing',
                subReportTitle: 'Processing Report-6 Description',
                subReportDescription: 'This is Report-6 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Processing GO',
                subReportButtonLink: '/report-6/1'
            },
        ],
        Tab_2_Reports: [
            {
                reportTitle: 'technical report Processing',
                reportLink: '/technical-report-1',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-1 Processing',
                subReportTitle: 'Processing Report-1 Description',
                subReportDescription: 'This is Report-1 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Processing GO',
                subReportButtonLink: '/technical-report-1/1'
            },
            {
                reportTitle: 'technical report Processing',
                reportLink: '/technical-report-2',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-2 Processing',
                subReportTitle: 'Processing Report-2 Description',
                subReportDescription: 'This is Report-2 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Processing GO',
                subReportButtonLink: '/technical-report-2/1'
            },
            {
                reportTitle: 'technical report Processing',
                reportLink: '/technical-report-3',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-3 Processing',
                subReportTitle: 'Processing Report-3 Description',
                subReportDescription: 'This is Report-3 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Processing GO',
                subReportButtonLink: '/technical-report-3/1'
            },
            {
                reportTitle: 'technical report Processing',
                reportLink: '/technical-report-4',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-4 Processing',
                subReportTitle: 'Processing Report-4 Description',
                subReportDescription: 'This is Report-4 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Processing GO',
                subReportButtonLink: '/technical-report-4/1'
            },
            {
                reportTitle: 'technical report Processing',
                reportLink: '/technical-report-5',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-5 Processing',
                subReportTitle: 'Processing Report-5 Description',
                subReportDescription: 'This is Report-5 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Processing GO',
                subReportButtonLink: '/technical-report-5/1'
            },
            {
                reportTitle: 'technical report Processing',
                reportLink: '/technical-report-6',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-6 Processing',
                subReportTitle: 'Processing Report-6 Description',
                subReportDescription: 'This is Report-6 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Processing GO',
                subReportButtonLink: '/technical-report-6/1'
            },
        ],
        Tab_3_Reports: [
            {
                reportTitle: 'behavior report Processing',
                reportLink: '/behavior-report-1',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-1 Processing',
                subReportTitle: 'Processing Report-1 Description',
                subReportDescription: 'This is Report-1 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Processing GO',
                subReportButtonLink: '/behavior-report-1/1'
            },
            {
                reportTitle: 'behavior report Processing',
                reportLink: '/behavior-report-2',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-2 Processing',
                subReportTitle: 'Processing Report-2 Description',
                subReportDescription: 'This is Report-2 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Processing GO',
                subReportButtonLink: '/behavior-report-2/1'
            },
            {
                reportTitle: 'behavior report Processing',
                reportLink: '/behavior-report-3',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-3 Processing',
                subReportTitle: 'Processing Report-3 Description',
                subReportDescription: 'This is Report-3 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Processing GO',
                subReportButtonLink: '/behavior-report-3/1'
            },
            {
                reportTitle: 'behavior report Processing',
                reportLink: '/behavior-report-4',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-4 Processing',
                subReportTitle: 'Processing Report-4 Description',
                subReportDescription: 'This is Report-4 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Processing GO',
                subReportButtonLink: '/behavior-report-4/1'
            },
            {
                reportTitle: 'behavior report Processing',
                reportLink: '/behavior-report-5',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-5 Processing',
                subReportTitle: 'Processing Report-5 Description',
                subReportDescription: 'This is Report-5 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Processing GO',
                subReportButtonLink: '/behavior-report-5/1'
            },
            {
                reportTitle: 'behavior report Processing',
                reportLink: '/behavior-report-6',
                reportDescription: 'description report Processing',
                reportButtonTitle: 'click to see report-6 Processing',
                subReportTitle: 'Processing Report-6 Description',
                subReportDescription: 'This is Report-6 from Processing, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Processing GO',
                subReportButtonLink: '/behavior-report-6/1'
            },
        ],
    },
    vision: {
        id: 'Vision',
        link: 'vision',
        page: {
            tab1: {
                title: 'business',
                tabDescription: 'This is a Business Tab of Vision',
                id: 'business',
            },
            tab2: {
                title: 'technical',
                tabDescription: 'This is a Technical Tab of Vision',
                id: 'technical',
            },
            tab3: {
                title: 'behavior',
                tabDescription: 'This is a Behavior Tab of Vision',
                id: 'behavior',
            },
            orderTab: [tab1, tab2, tab3],
        },
        Tab_1_Reports: [
            {
                reportTitle: 'business report Vision',
                reportLink: '/business-report-1',
                reportDescription: 'description  business report Vision',
                reportButtonTitle: 'click to see business report-1 Vision',
                subReportTitle: 'Vision business Report-1 Description',
                subReportDescription: 'This is  business Report-1 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Vision GO',
                subReportButtonLink: '/business-report-1/1'
            },
            {
                reportTitle: 'business report Vision',
                reportLink: '/business-report-2',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-2 Vision',
                subReportTitle: 'Vision Report-2 Description',
                subReportDescription: 'This is Report-2 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Vision GO',
                subReportButtonLink: '/business-report-2/1'
            },
            {
                reportTitle: 'business report Vision',
                reportLink: '/business-report-3',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-3 Vision',
                subReportTitle: 'Vision Report-3 Description',
                subReportDescription: 'This is Report-3 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Vision GO',
                subReportButtonLink: '/business-report-3/1'
            },
            {
                reportTitle: 'business report Vision',
                reportLink: '/business-report-4',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-4 Vision',
                subReportTitle: 'Vision Report-4 Description',
                subReportDescription: 'This is Report-4 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Vision GO',
                subReportButtonLink: '/report-4/1'
            },
            {
                reportTitle: 'business report Vision',
                reportLink: '/business-report-5',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-5 Vision',
                subReportTitle: 'Vision Report-5 Description',
                subReportDescription: 'This is Report-5 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Vision GO',
                subReportButtonLink: '/report-5/1'
            },
            {
                reportTitle: 'business report Vision',
                reportLink: '/business-report-6',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-6 Vision',
                subReportTitle: 'Vision Report-6 Description',
                subReportDescription: 'This is Report-6 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Vision GO',
                subReportButtonLink: '/report-6/1'
            },
        ],
        Tab_2_Reports: [
            {
                reportTitle: 'technical report Vision',
                reportLink: '/technical-report-1',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-1 Vision',
                subReportTitle: 'Vision Report-1 Description',
                subReportDescription: 'This is Report-1 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Vision GO',
                subReportButtonLink: '/technical-report-1/1'
            },
            {
                reportTitle: 'technical report Vision',
                reportLink: '/technical-report-2',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-2 Vision',
                subReportTitle: 'Vision Report-2 Description',
                subReportDescription: 'This is Report-2 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Vision GO',
                subReportButtonLink: '/technical-report-2/1'
            },
            {
                reportTitle: 'technical report Vision',
                reportLink: '/technical-report-3',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-3 Vision',
                subReportTitle: 'Vision Report-3 Description',
                subReportDescription: 'This is Report-3 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Vision GO',
                subReportButtonLink: '/technical-report-3/1'
            },
            {
                reportTitle: 'technical report Vision',
                reportLink: '/technical-report-4',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-4 Vision',
                subReportTitle: 'Vision Report-4 Description',
                subReportDescription: 'This is Report-4 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Vision GO',
                subReportButtonLink: '/technical-report-4/1'
            },
            {
                reportTitle: 'technical report Vision',
                reportLink: '/technical-report-5',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-5 Vision',
                subReportTitle: 'Vision Report-5 Description',
                subReportDescription: 'This is Report-5 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Vision GO',
                subReportButtonLink: '/technical-report-5/1'
            },
            {
                reportTitle: 'technical report Vision',
                reportLink: '/technical-report-6',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-6 Vision',
                subReportTitle: 'Vision Report-6 Description',
                subReportDescription: 'This is Report-6 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Vision GO',
                subReportButtonLink: '/technical-report-6/1'
            },
        ],
        Tab_3_Reports: [
            {
                reportTitle: 'behavior report Vision',
                reportLink: '/behavior-report-1',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-1 Vision',
                subReportTitle: 'Vision Report-1 Description',
                subReportDescription: 'This is Report-1 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-1 Vision GO',
                subReportButtonLink: '/behavior-report-1/1'
            },
            {
                reportTitle: 'behavior report Vision',
                reportLink: '/behavior-report-2',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-2 Vision',
                subReportTitle: 'Vision Report-2 Description',
                subReportDescription: 'This is Report-2 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-2 Vision GO',
                subReportButtonLink: '/behavior-report-2/1'
            },
            {
                reportTitle: 'behavior report Vision',
                reportLink: '/behavior-report-3',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-3 Vision',
                subReportTitle: 'Vision Report-3 Description',
                subReportDescription: 'This is Report-3 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-3 Vision GO',
                subReportButtonLink: '/behavior-report-3/1'
            },
            {
                reportTitle: 'behavior report Vision',
                reportLink: '/behavior-report-4',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-4 Vision',
                subReportTitle: 'Vision Report-4 Description',
                subReportDescription: 'This is Report-4 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-4 Vision GO',
                subReportButtonLink: '/behavior-report-4/1'
            },
            {
                reportTitle: 'behavior report Vision',
                reportLink: '/behavior-report-5',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-5 Vision',
                subReportTitle: 'Vision Report-5 Description',
                subReportDescription: 'This is Report-5 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-5 Vision GO',
                subReportButtonLink: '/behavior-report-5/1'
            },
            {
                reportTitle: 'behavior report Vision',
                reportLink: '/behavior-report-6',
                reportDescription: 'description report Vision',
                reportButtonTitle: 'click to see report-6 Vision',
                subReportTitle: 'Vision Report-6 Description',
                subReportDescription: 'This is Report-6 from Vision, Please click to button to see it',
                subReportButtonTitle: 'Report-6 Vision GO',
                subReportButtonLink: '/behavior-report-6/1'
            },
        ],
        watcher: {
            id: 'Watcher',
            link: 'watcher',
            page: {
                tab1: {
                    title: 'business',
                    tabDescription: 'This is a Business Tab of Watcher',
                    id: 'business',
                },
                tab2: {
                    title: 'technical',
                    tabDescription: 'This is a Technical Tab of Watcher',
                    id: 'technical',
                },
                tab3: {
                    title: 'behavior',
                    tabDescription: 'This is a Behavior Tab of Watcher',
                    id: 'behavior',
                },
                orderTab: [tab1, tab2, tab3],
            },
            Tab_1_Reports: [
                {
                    reportTitle: 'business report Watcher',
                    reportLink: '/business-report-1',
                    reportDescription: 'description  business report Watcher',
                    reportButtonTitle: 'click to see business report-1 Watcher',
                    subReportTitle: 'Watcher business Report-1 Description',
                    subReportDescription: 'This is  business Report-1 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-1 Watcher GO',
                    subReportButtonLink: '/business-report-1/1'
                },
                {
                    reportTitle: 'business report Watcher',
                    reportLink: '/business-report-2',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-2 Watcher',
                    subReportTitle: 'Watcher Report-2 Description',
                    subReportDescription: 'This is Report-2 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-2 Watcher GO',
                    subReportButtonLink: '/business-report-2/1'
                },
                {
                    reportTitle: 'business report Watcher',
                    reportLink: '/business-report-3',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-3 Watcher',
                    subReportTitle: 'Watcher Report-3 Description',
                    subReportDescription: 'This is Report-3 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-3 Watcher GO',
                    subReportButtonLink: '/business-report-3/1'
                },
                {
                    reportTitle: 'business report Watcher',
                    reportLink: '/business-report-4',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-4 Watcher',
                    subReportTitle: 'Watcher Report-4 Description',
                    subReportDescription: 'This is Report-4 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-4 Watcher GO',
                    subReportButtonLink: '/report-4/1'
                },
                {
                    reportTitle: 'business report Watcher',
                    reportLink: '/business-report-5',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-5 Watcher',
                    subReportTitle: 'Watcher Report-5 Description',
                    subReportDescription: 'This is Report-5 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-5 Watcher GO',
                    subReportButtonLink: '/report-5/1'
                },
                {
                    reportTitle: 'business report Watcher',
                    reportLink: '/business-report-6',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-6 Watcher',
                    subReportTitle: 'Watcher Report-6 Description',
                    subReportDescription: 'This is Report-6 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-6 Watcher GO',
                    subReportButtonLink: '/report-6/1'
                },
            ],
            Tab_2_Reports: [
                {
                    reportTitle: 'technical report Watcher',
                    reportLink: '/technical-report-1',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-1 Watcher',
                    subReportTitle: 'Watcher Report-1 Description',
                    subReportDescription: 'This is Report-1 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-1 Watcher GO',
                    subReportButtonLink: '/technical-report-1/1'
                },
                {
                    reportTitle: 'technical report Watcher',
                    reportLink: '/technical-report-2',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-2 Watcher',
                    subReportTitle: 'Watcher Report-2 Description',
                    subReportDescription: 'This is Report-2 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-2 Watcher GO',
                    subReportButtonLink: '/technical-report-2/1'
                },
                {
                    reportTitle: 'technical report Watcher',
                    reportLink: '/technical-report-3',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-3 Watcher',
                    subReportTitle: 'Watcher Report-3 Description',
                    subReportDescription: 'This is Report-3 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-3 Watcher GO',
                    subReportButtonLink: '/technical-report-3/1'
                },
                {
                    reportTitle: 'technical report Watcher',
                    reportLink: '/technical-report-4',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-4 Watcher',
                    subReportTitle: 'Watcher Report-4 Description',
                    subReportDescription: 'This is Report-4 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-4 Watcher GO',
                    subReportButtonLink: '/technical-report-4/1'
                },
                {
                    reportTitle: 'technical report Watcher',
                    reportLink: '/technical-report-5',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-5 Watcher',
                    subReportTitle: 'Watcher Report-5 Description',
                    subReportDescription: 'This is Report-5 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-5 Watcher GO',
                    subReportButtonLink: '/technical-report-5/1'
                },
                {
                    reportTitle: 'technical report Watcher',
                    reportLink: '/technical-report-6',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-6 Watcher',
                    subReportTitle: 'Watcher Report-6 Description',
                    subReportDescription: 'This is Report-6 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-6 Watcher GO',
                    subReportButtonLink: '/technical-report-6/1'
                },
            ],
            Tab_3_Reports: [
                {
                    reportTitle: 'behavior report Watcher',
                    reportLink: '/behavior-report-1',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-1 Watcher',
                    subReportTitle: 'Watcher Report-1 Description',
                    subReportDescription: 'This is Report-1 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-1 Watcher GO',
                    subReportButtonLink: '/behavior-report-1/1'
                },
                {
                    reportTitle: 'behavior report Watcher',
                    reportLink: '/behavior-report-2',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-2 Watcher',
                    subReportTitle: 'Watcher Report-2 Description',
                    subReportDescription: 'This is Report-2 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-2 Watcher GO',
                    subReportButtonLink: '/behavior-report-2/1'
                },
                {
                    reportTitle: 'behavior report Watcher',
                    reportLink: '/behavior-report-3',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-3 Watcher',
                    subReportTitle: 'Watcher Report-3 Description',
                    subReportDescription: 'This is Report-3 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-3 Watcher GO',
                    subReportButtonLink: '/behavior-report-3/1'
                },
                {
                    reportTitle: 'behavior report Watcher',
                    reportLink: '/behavior-report-4',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-4 Watcher',
                    subReportTitle: 'Watcher Report-4 Description',
                    subReportDescription: 'This is Report-4 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-4 Watcher GO',
                    subReportButtonLink: '/behavior-report-4/1'
                },
                {
                    reportTitle: 'behavior report Watcher',
                    reportLink: '/behavior-report-5',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-5 Watcher',
                    subReportTitle: 'Watcher Report-5 Description',
                    subReportDescription: 'This is Report-5 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-5 Watcher GO',
                    subReportButtonLink: '/behavior-report-5/1'
                },
                {
                    reportTitle: 'behavior report Watcher',
                    reportLink: '/behavior-report-6',
                    reportDescription: 'description report Watcher',
                    reportButtonTitle: 'click to see report-6 Watcher',
                    subReportTitle: 'Watcher Report-6 Description',
                    subReportDescription: 'This is Report-6 from Watcher, Please click to button to see it',
                    subReportButtonTitle: 'Report-6 Watcher GO',
                    subReportButtonLink: '/behavior-report-6/1'
                },
            ],
        },
        antarctica: {
            id: 'Antarctica',
            link: 'antarctica',
            page: {
                tab1: {
                    title: 'business',
                    tabDescription: 'This is a Business Tab of Antarctica',
                    id: 'business',
                },
                tab2: {
                    title: 'technical',
                    tabDescription: 'This is a Technical Tab of Antarctica',
                    id: 'technical',
                },
                tab3: {
                    title: 'behavior',
                    tabDescription: 'This is a Behavior Tab of Antarctica',
                    id: 'behavior',
                },
                orderTab: [tab1, tab2, tab3],
            },
            Tab_1_Reports: [
                {
                    reportTitle: 'business report Antarctica',
                    reportLink: '/business-report-1',
                    reportDescription: 'description  business report Antarctica',
                    reportButtonTitle: 'click to see business report-1 Antarctica',
                    subReportTitle: 'Antarctica business Report-1 Description',
                    subReportDescription: 'This is  business Report-1 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-1 Antarctica GO',
                    subReportButtonLink: '/business-report-1/1'
                },
                {
                    reportTitle: 'business report Antarctica',
                    reportLink: '/business-report-2',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-2 Antarctica',
                    subReportTitle: 'Antarctica Report-2 Description',
                    subReportDescription: 'This is Report-2 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-2 Antarctica GO',
                    subReportButtonLink: '/business-report-2/1'
                },
                {
                    reportTitle: 'business report Antarctica',
                    reportLink: '/business-report-3',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-3 Antarctica',
                    subReportTitle: 'Antarctica Report-3 Description',
                    subReportDescription: 'This is Report-3 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-3 Antarctica GO',
                    subReportButtonLink: '/business-report-3/1'
                },
                {
                    reportTitle: 'business report Antarctica',
                    reportLink: '/business-report-4',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-4 Antarctica',
                    subReportTitle: 'Antarctica Report-4 Description',
                    subReportDescription: 'This is Report-4 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-4 Antarctica GO',
                    subReportButtonLink: '/report-4/1'
                },
                {
                    reportTitle: 'business report Antarctica',
                    reportLink: '/business-report-5',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-5 Antarctica',
                    subReportTitle: 'Antarctica Report-5 Description',
                    subReportDescription: 'This is Report-5 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-5 Antarctica GO',
                    subReportButtonLink: '/report-5/1'
                },
                {
                    reportTitle: 'business report Antarctica',
                    reportLink: '/business-report-6',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-6 Antarctica',
                    subReportTitle: 'Antarctica Report-6 Description',
                    subReportDescription: 'This is Report-6 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-6 Antarctica GO',
                    subReportButtonLink: '/report-6/1'
                },
            ],
            Tab_2_Reports: [
                {
                    reportTitle: 'technical report Antarctica',
                    reportLink: '/technical-report-1',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-1 Antarctica',
                    subReportTitle: 'Antarctica Report-1 Description',
                    subReportDescription: 'This is Report-1 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-1 Antarctica GO',
                    subReportButtonLink: '/technical-report-1/1'
                },
                {
                    reportTitle: 'technical report Antarctica',
                    reportLink: '/technical-report-2',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-2 Antarctica',
                    subReportTitle: 'Antarctica Report-2 Description',
                    subReportDescription: 'This is Report-2 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-2 Antarctica GO',
                    subReportButtonLink: '/technical-report-2/1'
                },
                {
                    reportTitle: 'technical report Antarctica',
                    reportLink: '/technical-report-3',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-3 Antarctica',
                    subReportTitle: 'Antarctica Report-3 Description',
                    subReportDescription: 'This is Report-3 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-3 Antarctica GO',
                    subReportButtonLink: '/technical-report-3/1'
                },
                {
                    reportTitle: 'technical report Antarctica',
                    reportLink: '/technical-report-4',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-4 Antarctica',
                    subReportTitle: 'Antarctica Report-4 Description',
                    subReportDescription: 'This is Report-4 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-4 Antarctica GO',
                    subReportButtonLink: '/technical-report-4/1'
                },
                {
                    reportTitle: 'technical report Antarctica',
                    reportLink: '/technical-report-5',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-5 Antarctica',
                    subReportTitle: 'Antarctica Report-5 Description',
                    subReportDescription: 'This is Report-5 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-5 Antarctica GO',
                    subReportButtonLink: '/technical-report-5/1'
                },
                {
                    reportTitle: 'technical report Antarctica',
                    reportLink: '/technical-report-6',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-6 Antarctica',
                    subReportTitle: 'Antarctica Report-6 Description',
                    subReportDescription: 'This is Report-6 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-6 Antarctica GO',
                    subReportButtonLink: '/technical-report-6/1'
                },
            ],
            Tab_3_Reports: [
                {
                    reportTitle: 'behavior report Antarctica',
                    reportLink: '/behavior-report-1',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-1 Antarctica',
                    subReportTitle: 'Antarctica Report-1 Description',
                    subReportDescription: 'This is Report-1 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-1 Antarctica GO',
                    subReportButtonLink: '/behavior-report-1/1'
                },
                {
                    reportTitle: 'behavior report Antarctica',
                    reportLink: '/behavior-report-2',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-2 Antarctica',
                    subReportTitle: 'Antarctica Report-2 Description',
                    subReportDescription: 'This is Report-2 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-2 Antarctica GO',
                    subReportButtonLink: '/behavior-report-2/1'
                },
                {
                    reportTitle: 'behavior report Antarctica',
                    reportLink: '/behavior-report-3',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-3 Antarctica',
                    subReportTitle: 'Antarctica Report-3 Description',
                    subReportDescription: 'This is Report-3 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-3 Antarctica GO',
                    subReportButtonLink: '/behavior-report-3/1'
                },
                {
                    reportTitle: 'behavior report Antarctica',
                    reportLink: '/behavior-report-4',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-4 Antarctica',
                    subReportTitle: 'Antarctica Report-4 Description',
                    subReportDescription: 'This is Report-4 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-4 Antarctica GO',
                    subReportButtonLink: '/behavior-report-4/1'
                },
                {
                    reportTitle: 'behavior report Antarctica',
                    reportLink: '/behavior-report-5',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-5 Antarctica',
                    subReportTitle: 'Antarctica Report-5 Description',
                    subReportDescription: 'This is Report-5 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-5 Antarctica GO',
                    subReportButtonLink: '/behavior-report-5/1'
                },
                {
                    reportTitle: 'behavior report Antarctica',
                    reportLink: '/behavior-report-6',
                    reportDescription: 'description report Antarctica',
                    reportButtonTitle: 'click to see report-6 Antarctica',
                    subReportTitle: 'Antarctica Report-6 Description',
                    subReportDescription: 'This is Report-6 from Antarctica, Please click to button to see it',
                    subReportButtonTitle: 'Report-6 Antarctica GO',
                    subReportButtonLink: '/behavior-report-6/1'
                },
            ],
        },
    },
}








