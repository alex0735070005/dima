
export default function subCategory (state = {}, action)
{
   
    switch(action.type)
    {
        case 'UPDATE_SUBCATEGORY':
        return {
          ...state,
          ...action.data
        }
    }
    return state;
}