import {combineReducers} from 'redux';

import menu from "./menu";
import categories from "./categories";
import category from "./category";
import subCategory from "./subCategory";

export default combineReducers({
    menu,
    categories,
    category,
    subCategory
});