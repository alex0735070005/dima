import React from 'react';

import { Route, Switch } from 'react-router-dom';

import Home from './components/pages/Home';
import Category from './components/pages/Category';


export default () => (
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/:slug' component={Category}/>
    </Switch>
);