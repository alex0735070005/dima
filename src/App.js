import React, {Component} from "react";

import Footer from "./components/static/Footer";
import Header from "./components/static/Header";
import Router from "./Router";
import styles from "./App.scss";

export default class App extends Component {

    render()
    {
        return (
            <div className="app-container container">
                <Header />
                <Router />
                <Footer />
            </div>
        )
    }
}